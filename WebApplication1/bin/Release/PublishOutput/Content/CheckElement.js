"use strict";
var system = require('system')
var page = require('webpage').create();
if (system.args.length < 2) {
    console.log('Missing url argument');
    phantom.exit(1);
}
var address = system.args[1];
console.log(address);
page.open(address);
page.onLoadFinished = function (status) {
    window.setTimeout(function () {
        var errors = page.evaluate(function () {
            var e = document.getElementById('everythingLoadedOk');
            if (e == null) {
                return false;
            } else {
                return true;
            }

        });
        console.log(errors);
        phantom.exit();
    }, 3000);
    // phantom.exit();
}