﻿using NLog;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using SelectPdf;
using System.Threading.Tasks;

namespace WebApplication1.Controllers
{
    public class PDFController : ApiController
    {
        // GET: api/PDF
        static string cutyPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/CutyCapt/CutyCapt.exe");
        static string phantomJSPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/phantomjs.exe");
        static string phantomScriptPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/CheckElement.js");
        static string tempPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/Temp.pdf");
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private bool CheckElementLoaded(string url)
        {
            string urlFormatted = "file:"+url.Replace(@"\", @"/");
            string args = "--local-to-remote-url-access=true \"" + phantomScriptPath + "\"" + " " + "\"" + urlFormatted + "\"";
            ProcessStartInfo info = new ProcessStartInfo(phantomJSPath)
            {
                Arguments = args,
                UseShellExecute = false,
                // we aren't looking to input anything, otherwise you will get a blank screen 
                // RedirectStandardInput = true,
                RedirectStandardError = true,
                RedirectStandardOutput = true
            };

            string output, error = string.Empty;
            try
            {
                using (Process phantomJS = Process.Start(info))

                {

                    output = phantomJS.StandardOutput.ReadToEnd();
                    error = phantomJS.StandardError.ReadToEnd();

                    phantomJS.WaitForExit(10000);

                    if (phantomJS.ExitCode != 0)
                    {
                        logger.Error(string.Format("PhantomJS exited with a non zero exit code ({0}), " +
                            "check to see if #everythingLoadedOk tag is present: {1}", phantomJS.ExitCode, error));
                        throw new HttpResponseException(
                            new HttpResponseMessage(HttpStatusCode.NotFound)
                            {
                                Content = new StringContent(error)
                            });
                    }
                    logger.Info("PhantomJS Output: {0} \nPhantomJS Error: {1}",output,error);
                    if (output.Contains("true"))
                    {
                        return true;
                    } else
                    {
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                if (e is HttpResponseException)
                {
                    throw e;
                }
                else
                {
                    logger.Error(e, "An error occurred when trying to run the PhantomJS process");
                    throw new HttpResponseException(
                        new HttpResponseMessage(HttpStatusCode.InternalServerError)
                        {
                            Content = new StringContent(error)
                        });
                }
            }
        }


        public string[] Get(string id)
        {
           
            string decodedString = "";
            try
            {
                byte[] data = Convert.FromBase64String(id);              
                decodedString = Encoding.UTF8.GetString(data).Trim();
                logger.Info(string.Format("Received base64 encoded parameter: {0} \ndecoded to: {1}", id, decodedString));
            } catch(Exception e)
            {
                logger.Error(e,"There was an error when decoding the parameter from Base64");
                throw new HttpResponseException(
                    new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent(string.Format("Not a valid Base64 string, detailed error: {0}",e.Message))
                    });
            }
            string[] stringArgs = decodedString.Split(',');

            if (stringArgs.Length != 3)
            {
                logger.Error("The length of the arguments provided is less than 3");
                throw new HttpResponseException(
                    new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent(string.Format("The number of arguments supplied is invalid"))
                    });
            }

            string originalPath = stringArgs[0];
            if (!CheckElementLoaded(originalPath))
            {
                logger.Error("Element #everythingLoadedOk was not found, will skip creation of PDF document");
                throw new HttpResponseException(
                    new HttpResponseMessage(HttpStatusCode.NotFound)
                    {
                        Content = new StringContent("Element everythingLoadedOk was not found")
                    });
            }

            string destinationPath = stringArgs[1];
            string appId = stringArgs[2];
            //LogManager.Configuration.Variables["id"] = appId;
            if (!File.Exists(originalPath))
            {
                logger.Error(string.Format("The path requested {0} was not found", originalPath));
                throw new HttpResponseException(
                    new HttpResponseMessage(HttpStatusCode.NotFound)
                    {
                        Content = new StringContent(string.Format("The path '{0}' was not found", originalPath))
                    });
            }

            try
            {
                (new FileInfo(destinationPath)).Directory.Create();
            }
            catch (Exception e)
            {
                logger.Error(e, string.Format("Unable to create directory for output path: {0}", destinationPath));
                throw new HttpResponseException(
                    new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(string.Format("Unable to create directory for output path: {0}", destinationPath))
                    });
            }

            //file path must use the URL convention for slashes (forward slash) 

            string pdf_page_size = "A4";
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
                pdf_page_size, true);

            string pdf_orientation = "Portrait";
            PdfPageOrientation pdfOrientation =
                (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                pdf_orientation, true);
            int webPageWidth = 800;
            int webPageHeight = 0;

            // instantiate a html to pdf converter object
            SelectPdf.HtmlToPdf converter = new SelectPdf.HtmlToPdf();

            // set converter options
            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            converter.Options.WebPageHeight = webPageHeight;
            converter.Options.DrawBackground = false;
            converter.Options.MinPageLoadTime = 2;
            converter.Options.KeepTextsTogether = true;
            converter.Options.KeepImagesTogether = true;
            converter.Options.MarginRight = 30;
            converter.Options.MarginLeft = 30;
            converter.Options.MarginBottom = 40;
            converter.Options.MarginTop = 40;
            // create a new pdf document converting an url

            SelectPdf.PdfDocument doc = converter.ConvertUrl(originalPath);
          
            doc.Save(destinationPath);

                

            // close pdf document
            doc.Close();



            
            return new string[] { originalPath, destinationPath, appId };
        }

        // POST: api/PDF
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/PDF/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/PDF/5
        public void Delete(int id)
        {
        }

        private void cutyCapt(string originalPath, string destinationPath)
        {



            string args = string.Format("--url=\"file:{0}\" --out=\"{1}\" --delay=1500 --assert-element=\"#everythingLoadedOk\"", originalPath.Replace(@"\", @"/"), destinationPath);

#if DEBUG
            args = string.Format("--url=\"file:{0}\" --out=\"{1}\" --delay=1500", originalPath.Replace(@"\", @"/"), destinationPath);
#endif
            logger.Info("CutyCapt generated arguments: " + args);

            ProcessStartInfo info = new ProcessStartInfo(cutyPath)

            {
                Arguments = args,
                UseShellExecute = false,
                // we aren't looking to input anything, otherwise you will get a blank screen 
                // RedirectStandardInput = true,
                RedirectStandardError = true,
                RedirectStandardOutput = true
            };

            string output, error = string.Empty;
            try
            {
                using (Process cutyCapt = Process.Start(info))

                {

                    output = cutyCapt.StandardOutput.ReadToEnd();
                    error = cutyCapt.StandardError.ReadToEnd();

                    cutyCapt.WaitForExit(10000);

                    if (cutyCapt.ExitCode != 0)
                    {
                        logger.Error(string.Format("CutyCapt exited with a non zero exit code ({0}), " +
                            "check to see if #everythingLoadedOk tag is present: {1}", cutyCapt.ExitCode, error));
                        throw new HttpResponseException(
                            new HttpResponseMessage(HttpStatusCode.BadRequest)
                            {
                                Content = new StringContent(error)
                            });
                    }

                }
            }
            catch (Exception e)
            {
                if (e is HttpResponseException)
                {
                    throw e;
                }
                else
                {
                    logger.Error(e, "An error occurred when trying to run the CutyCapt process");
                    throw new HttpResponseException(
                        new HttpResponseMessage(HttpStatusCode.InternalServerError)
                        {
                            Content = new StringContent(error)
                        });
                }
            }
        }
    }
}
